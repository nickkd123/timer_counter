Counter Capture Driver
================================================

Building the kernel
-------------------
```
make CROSS_COMPILE=arm-linux-gnueabihf- KDIR=/path/to/SCADA/board-support/kernel
```

Installing the Device Tree Overlay
----------------------------------
**Remove** am335xx-test_module_timer node:
```
	am33xx-test_module_timer {
		compatible = "test,am33xx-test_module_timer";
		pinctrl-names = "default";
		pinctrl-0 = <&timer5_pin>;
		status = "okay";
		DMtimer = <&timer5>;
	};

```

Then **add** following nodes,

```
	am33xx-timer_capture@5 {
		compatible = "omap,am33xx-timer_capture";
		pinctrl-names = "default";
		pinctrl-0 = <&timer5_pin>;
		status = "okay";
		DMtimer = <&timer5>;
	};

	am33xx-timer_capture@6 {
		compatible = "omap,am33xx-timer_capture";
		pinctrl-names = "default";
		pinctrl-0 = <&timer6_pin>;
		status = "okay";
		DMtimer = <&timer6>;
	};
```

Monitoring operation
--------------------

The sysfs files in /sys/devices/platform/am33xx-timer_capture@* contain the counter's current state:

 * capture [READ] - the number of detected event/edge.
 * direction [READ|WRITE] - set/get type of edge: rising, falling, both.
 * control [READ|WRITE] - get/set current state of timer: start, stop, reset
 * count\_at\_interrupt - the counter's value at interrupt time
 * interrupt\_delta - the value used for the interrupt latency offset
 * pps\_ts - the final timestamp value sent to the pps system
 * timer\_counter - the raw counter value
 * stats - the number of captures and timer overflows
 * timer\_name - the name of time timer hardware
 * ctrlstatus - the state of the TCLR register (see the AM335x Technical Reference Manual for bit meanings)

Example usage
-------------

* set timer 5 to capture falling edge
```
# echo 'falling' > /sys/devices/platform/am33xx-timer_capture@5/direction
```

* get number of event in timer 5
```
# cat /sys/devices/platform/am33xx-timer_capture@5/capture
```

* clear capture value of timer 5
```
# echo 'reset' > /sys/devices/platform/am33xx-timer_capture@5/control
```

* set timer 6 to capture both rising & falling edge
```
# echo 'both' > /sys/devices/platform/am33xx-timer_capture@6/direction
```
