/*
 * timer_counter.c -- Timer counter driver for BBB Omap
 *
 * Copyright (C) 2018  Linh Nguyen <nvl1109@gmail.com>
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/string.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/pinctrl/consumer.h>
#include <linux/pps_kernel.h>
#include <linux/clocksource.h>
#include <linux/time.h>
#include <linux/pm.h>

#include "dmtimer.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Linh Nguyen");
MODULE_DESCRIPTION("Timer counter capture driver for OMAP BBB");
MODULE_VERSION("0.1.0");

#define MODULE_NAME "timer_counter"
#define TMR_DEBUG(fmt, args...) \
	printk(KERN_DEBUG MODULE_NAME ":%s:%d " fmt "\n", __FUNCTION__, __LINE__, ##args)
#define TMR_INFO(fmt, args...) \
	printk(KERN_INFO MODULE_NAME ":%s:%d " fmt "\n", __FUNCTION__, __LINE__, ##args)
#define TMR_ERR(fmt, args...) \
	printk(KERN_ERR MODULE_NAME ":%s:%d " fmt "\n", __FUNCTION__, __LINE__, ##args)

typedef enum {
	DIRECTION_RISING = 0,
	DIRECTION_FALLING,
	DIRECTION_BOTH,
	DIRECTION_MAX,
} capture_direction_t;

typedef enum {
	TMRCPT_STATE_STOP = 0,
	TMRCPT_STATE_STARTED,
	TMRCPT_STATE_MAX,
} tmrcpt_state_t;

struct omap_tmrcpt_platform_data {
	struct omap_dm_timer *capture_timer;
	const char *timer_name;
	uint32_t frequency;
	unsigned int capture;
	unsigned int overflow;
	unsigned int count_at_interrupt;
	int direction;
	struct pps_event_time ts;
	struct timespec64 delta;
	struct pps_device *pps;
	struct pps_source_info info;
	int ready;
	struct clocksource clksrc;
	int state;
};

/* Prototypes */
static void omap_tmrcpt_cleanup_timer(struct omap_tmrcpt_platform_data *pdata);
static void omap_tmrcpt_setup_capture(struct omap_dm_timer *timer, int direction);

/* kobject *******************/
static ssize_t timer_name_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%s\n", pdata->timer_name);
}

static DEVICE_ATTR(timer_name, S_IRUGO, timer_name_show, NULL);

static ssize_t direction_store(struct device *dev, struct device_attribute *attr,
	const char *buf, size_t count)
{
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	capture_direction_t dir = DIRECTION_MAX;
	char tmp[20];
	int cnt = count;
	if (count > 19) {
		cnt = 19;
	}
	snprintf(tmp, cnt, "%s", buf);

	if (memcmp(buf, "rising", 6) == 0) {
		dir = DIRECTION_RISING;
	} else if (0 == memcmp(buf, "falling", 7)) {
		dir = DIRECTION_FALLING;
	} else if (0 == memcmp(buf, "both", 4)) {
		dir = DIRECTION_BOTH;
	} else {
		TMR_ERR("direction [%s] is UNKNOWN", tmp);

		return count;
	}

	TMR_INFO("set direction to %d-%s", dir, tmp);
	pdata->direction = dir;
	// Restart the timer
	omap_dm_timer_set_int_disable(pdata->capture_timer, OMAP_TIMER_INT_CAPTURE|OMAP_TIMER_INT_OVERFLOW);
	// omap_dm_timer_stop(pdata->capture_timer);
	pdata->capture = 0;
	omap_tmrcpt_setup_capture(pdata->capture_timer, pdata->direction);
	pdata->state = TMRCPT_STATE_STARTED;

	return count;
}

static ssize_t direction_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	int count = 0;

	TMR_DEBUG("direction: %d", pdata->direction);
	switch (pdata->direction) {
		case DIRECTION_RISING:
			count = sprintf(buf, "rising\n");
		break;

		case DIRECTION_FALLING:
			count = sprintf(buf, "falling\n");
		break;

		case DIRECTION_BOTH:
			count = sprintf(buf, "both\n");
		break;

		default:
			count = sprintf(buf, "UNKNOWN\n");
		break;
	}

	return count;
}

static DEVICE_ATTR(direction, S_IWUSR|S_IWGRP|S_IRUGO, direction_show, direction_store);

static ssize_t control_store(struct device *dev, struct device_attribute *attr,
	const char *buf, size_t count)
{
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	char tmp[20];
	int cnt = count;
	if (count > 19) {
		cnt = 19;
	}
	snprintf(tmp, cnt, "%s", buf);

	TMR_DEBUG("control action: %s", buf);

	if (0 == memcmp(buf, "start", 5)) {
		// Start timer
		pdata->state = TMRCPT_STATE_STARTED;
		pdata->capture = 0;
	} else if (0 == memcmp(buf, "stop", 4)) {
		// stop timer
		pdata->state = TMRCPT_STATE_STOP;
	} else if (0 == memcmp(buf, "reset", 5)) {
		// Reset capture value
		pdata->capture = 0;
	} else {
		TMR_ERR("control action %s invalid", tmp);
	}

	return count;
}

static ssize_t control_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;

	return sprintf(buf, "state: %s\n", (pdata->state == TMRCPT_STATE_STARTED) ? "started" : "stoped");
}

static DEVICE_ATTR(control, S_IWUSR|S_IWGRP|S_IRUGO, control_show, control_store);

static ssize_t stats_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "capture: %u\noverflow: %u\n", pdata->capture, pdata->overflow);
}

static DEVICE_ATTR(stats, S_IRUGO, stats_show, NULL);

static ssize_t interrupt_delta_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%lld.%09ld\n", (long long)pdata->delta.tv_sec, pdata->delta.tv_nsec);
}

static DEVICE_ATTR(interrupt_delta, S_IRUGO, interrupt_delta_show, NULL);

static ssize_t pps_ts_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%lld.%09ld\n", (long long)pdata->ts.ts_real.tv_sec, pdata->ts.ts_real.tv_nsec);
}

static DEVICE_ATTR(pps_ts, S_IRUGO, pps_ts_show, NULL);

static ssize_t count_at_interrupt_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%u\n", pdata->count_at_interrupt);
}

static DEVICE_ATTR(count_at_interrupt, S_IRUGO, count_at_interrupt_show, NULL);

static ssize_t capture_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%u\n", pdata->capture);
}

static DEVICE_ATTR(capture, S_IRUGO, capture_show, NULL);

static ssize_t ctrlstatus_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	return sprintf(buf, "%x\n",
			__omap_dm_timer_read(pdata->capture_timer, OMAP_TIMER_CTRL_REG, pdata->capture_timer->posted)
			);
}

static DEVICE_ATTR(ctrlstatus, S_IRUGO, ctrlstatus_show, NULL);

static ssize_t timer_counter_show(struct device *dev, struct device_attribute *attr, char *buf) {
	struct omap_tmrcpt_platform_data *pdata = dev->platform_data;
	unsigned int current_count = 0;

	current_count = omap_dm_timer_read_counter(pdata->capture_timer);
	return sprintf(buf, "%u\n", current_count);
}

static DEVICE_ATTR(timer_counter, S_IRUGO, timer_counter_show, NULL);

static struct attribute *attrs[] = {
	 &dev_attr_timer_counter.attr,
	 &dev_attr_ctrlstatus.attr,
	 &dev_attr_capture.attr,
	 &dev_attr_count_at_interrupt.attr,
	 &dev_attr_pps_ts.attr,
	 &dev_attr_interrupt_delta.attr,
	 &dev_attr_stats.attr,
	 &dev_attr_timer_name.attr,
	 &dev_attr_direction.attr,
	 &dev_attr_control.attr,
	 NULL,
};

static struct attribute_group attr_group = {
	 .attrs = attrs,
};

/* timers ********************/
static irqreturn_t omap_tmrcpt_interrupt(int irq, void *data) {
	struct omap_tmrcpt_platform_data *pdata;

	pdata = data;

	if(pdata->ready) {
		unsigned int irq_status;

		irq_status = omap_dm_timer_read_status(pdata->capture_timer);
		if(irq_status & OMAP_TIMER_INT_CAPTURE) {
			uint32_t ps_per_hz;
			unsigned int count_at_capture;

			pps_get_ts(&pdata->ts);
			pdata->count_at_interrupt = omap_dm_timer_read_counter(pdata->capture_timer);
			count_at_capture = __omap_dm_timer_read(pdata->capture_timer, OMAP_TIMER_CAPTURE_REG, pdata->capture_timer->posted);

			pdata->delta.tv_sec = 0;

			// use picoseconds per hz to avoid floating point and limit the rounding error
			ps_per_hz = 1000000000 / (pdata->frequency / 1000);
			pdata->delta.tv_nsec = ((pdata->count_at_interrupt - count_at_capture) * ps_per_hz) / 1000;

			pps_sub_ts(&pdata->ts, pdata->delta);
			pps_event(pdata->pps, &pdata->ts, PPS_CAPTUREASSERT, NULL);

			if (TMRCPT_STATE_STARTED == pdata->state) {
				pdata->capture++;
			}

			__omap_dm_timer_write_status(pdata->capture_timer, OMAP_TIMER_INT_CAPTURE);
		}
		if(irq_status & OMAP_TIMER_INT_OVERFLOW) {
			pdata->overflow++;
			__omap_dm_timer_write_status(pdata->capture_timer, OMAP_TIMER_INT_OVERFLOW);
		}
	}

	return IRQ_HANDLED; // TODO: shared interrupts?
}

static void omap_tmrcpt_setup_capture(struct omap_dm_timer *timer, int direction) {
	u32 ctrl;

	omap_dm_timer_set_source(timer, OMAP_TIMER_SRC_SYS_CLK);

	omap_dm_timer_enable(timer);

	ctrl = __omap_dm_timer_read(timer, OMAP_TIMER_CTRL_REG, timer->posted);

	// disable prescaler
	ctrl &= ~(OMAP_TIMER_CTRL_PRE | (0x07 << 2));

	// autoreload
	ctrl |= OMAP_TIMER_CTRL_AR;
	__omap_dm_timer_write(timer, OMAP_TIMER_LOAD_REG, 0, timer->posted);

	// start timer
	ctrl |= OMAP_TIMER_CTRL_ST;

	// set capture
	TMR_DEBUG("set direction to %d, ctrl now: 0x%x", direction, ctrl);
	ctrl &= ~OMAP_TIMER_CTRL_TCM_BOTHEDGES;
	switch(direction) {
		case DIRECTION_RISING:
			ctrl |= OMAP_TIMER_CTRL_TCM_LOWTOHIGH;
		break;

		case DIRECTION_FALLING:
			ctrl |= OMAP_TIMER_CTRL_TCM_HIGHTOLOW;
		break;

		case DIRECTION_BOTH:
			ctrl |= OMAP_TIMER_CTRL_TCM_BOTHEDGES;
		break;

		default:
			ctrl |= OMAP_TIMER_CTRL_TCM_LOWTOHIGH;
		break;
	}
	TMR_DEBUG("ctrl now: 0x%x", ctrl);
	ctrl |= OMAP_TIMER_CTRL_GPOCFG;

	__omap_dm_timer_load_start(timer, ctrl, 0, timer->posted);

	/* Save the context */
	timer->context.tclr = ctrl;
	timer->context.tldr = 0;
	timer->context.tcrr = 0;
}

/* if tclkin has no clock, writes to the timer registers will stall and you will get a message like:
 * Unhandled fault: external abort on non-linefetch (0x1028) at 0xfa044048
 */
static void omap_dm_timer_use_tclkin(struct omap_tmrcpt_platform_data *pdata) {
	struct clk *gt_fclk;

	omap_dm_timer_set_source(pdata->capture_timer, OMAP_TIMER_SRC_EXT_CLK);
	gt_fclk = omap_dm_timer_get_fclk(pdata->capture_timer);
	pdata->frequency = clk_get_rate(gt_fclk);
	TMR_INFO("timer(%s) switched to tclkin, rate=%uHz", pdata->timer_name, pdata->frequency);
}

static void omap_tmrcpt_configure_irq(struct omap_tmrcpt_platform_data *pdata,
        unsigned int imask)
{
    __omap_dm_timer_int_enable(pdata->capture_timer, imask);
    pdata->capture_timer->context.tier = imask;
    pdata->capture_timer->context.twer = imask;
}

#define omap_tmrcpt_enable_irq(d)  omap_tmrcpt_configure_irq(d, OMAP_TIMER_INT_CAPTURE|OMAP_TIMER_INT_OVERFLOW)
#define omap_tmrcpt_disable_irq(d) omap_tmrcpt_configure_irq(d, 0)

static int omap_tmrcpt_init_timer(struct device_node *timer_dn, struct omap_tmrcpt_platform_data *pdata) {
	struct clk *gt_fclk;

	of_property_read_string_index(timer_dn, "ti,hwmods", 0, &pdata->timer_name);
	if (!pdata->timer_name) {
		TMR_ERR("ti,hwmods property missing?");
		return -ENODEV;
	}

	pdata->capture_timer = omap_dm_timer_request_by_node(timer_dn);
	if(!pdata->capture_timer) {
		TMR_ERR("request_by_node failed");
		return -ENODEV;
	}

	// TODO: use devm_request_irq?
	if(request_irq(pdata->capture_timer->irq, omap_tmrcpt_interrupt, IRQF_TIMER, MODULE_NAME, pdata)) {
		TMR_ERR("cannot register IRQ %d", pdata->capture_timer->irq);
		return -EIO;
	}

	omap_tmrcpt_setup_capture(pdata->capture_timer, pdata->direction);
	pdata->capture = 0;
	pdata->state = TMRCPT_STATE_STARTED;

	gt_fclk = omap_dm_timer_get_fclk(pdata->capture_timer);
	pdata->frequency = clk_get_rate(gt_fclk);

	TMR_INFO("timer name=%s rate=%uHz", pdata->timer_name, pdata->frequency);

	return 0;
}

static void omap_tmrcpt_cleanup_timer(struct omap_tmrcpt_platform_data *pdata) {
	if(pdata->capture_timer) {
		omap_dm_timer_set_source(pdata->capture_timer, OMAP_TIMER_SRC_SYS_CLK); // in case TCLKIN is stopped during boot
		omap_dm_timer_set_int_disable(pdata->capture_timer, OMAP_TIMER_INT_CAPTURE|OMAP_TIMER_INT_OVERFLOW);
		free_irq(pdata->capture_timer->irq, pdata);
		omap_dm_timer_stop(pdata->capture_timer);
		omap_dm_timer_free(pdata->capture_timer);
		pdata->capture_timer = NULL;
		TMR_INFO("Exiting.");
	}
}

/* clocksource ***************/
static struct omap_tmrcpt_platform_data *clocksource_timer = NULL;

static cycle_t omap_tmrcpt_read_cycles(struct clocksource *cs) {
	return (cycle_t)__omap_dm_timer_read_counter(clocksource_timer->capture_timer, clocksource_timer->capture_timer->posted);
}

static void omap_tmrcpt_clocksource_init(struct omap_tmrcpt_platform_data *pdata) {
	if(!clocksource_timer) {
		pdata->clksrc.name = pdata->timer_name;

		pdata->clksrc.rating = 299;
		pdata->clksrc.read = omap_tmrcpt_read_cycles;
		pdata->clksrc.mask = CLOCKSOURCE_MASK(32);
		pdata->clksrc.flags = CLOCK_SOURCE_IS_CONTINUOUS;

		clocksource_timer = pdata;
		if (clocksource_register_hz(&pdata->clksrc, pdata->frequency)) {
			TMR_ERR("Could not register clocksource %s", pdata->clksrc.name);
			clocksource_timer = NULL;
		} else {
			TMR_INFO("clocksource: %s at %u Hz", pdata->clksrc.name, pdata->frequency);
		}
	}
}

static void omap_tmrcpt_clocksource_cleanup(struct omap_tmrcpt_platform_data *pdata) {
	if(pdata == clocksource_timer) {
		clocksource_unregister(&pdata->clksrc);
		clocksource_timer = NULL;
	}
}

/* module ********************/
static struct omap_tmrcpt_platform_data *of_get_omap_tmrcpt_pdata(struct platform_device *pdev) {
	struct device_node *np = pdev->dev.of_node, *timer_dn;
	struct omap_tmrcpt_platform_data *pdata;
	const __be32 *timer_phandle;

	pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
	if (!pdata)
		return NULL;

	pdata->ready = 0;

	timer_phandle = of_get_property(np, "DMtimer", NULL);
	if(!timer_phandle) {
		TMR_ERR("DMtimer property in devicetree null");
		goto fail;
	}

	timer_dn = of_find_node_by_phandle(be32_to_cpup(timer_phandle));
	if(!timer_dn) {
		TMR_ERR("find_node_by_phandle failed");
		goto fail;
	}

	if(omap_tmrcpt_init_timer(timer_dn, pdata) < 0) {
		goto fail2;
	}

	of_node_put(timer_dn);

	return pdata;

fail2:
	of_node_put(timer_dn);
fail:
	devm_kfree(&pdev->dev, pdata);
	return NULL;
}

static const struct of_device_id omap_timer_capture_dt_ids[] = {
	{ .compatible = "omap,am33xx-timer_capture", },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, omap_timer_capture_dt_ids);

static int omap_timer_capture_probe(struct platform_device *pdev) {
	const struct of_device_id *match;
	struct omap_tmrcpt_platform_data *pdata;
	struct pinctrl *pinctrl;
	const __be32 *use_tclkin;

	match = of_match_device(omap_timer_capture_dt_ids, &pdev->dev);
	if (match) {
		pdev->dev.platform_data = of_get_omap_tmrcpt_pdata(pdev);
	} else {
		TMR_ERR("of_match_device failed");
	}
	pdata = pdev->dev.platform_data;
	if(!pdata)
		return -ENODEV;

	pdata->ready = 0;

	if(sysfs_create_group(&pdev->dev.kobj, &attr_group)) {
		TMR_ERR("sysfs_create_group failed");
	}

	pinctrl = devm_pinctrl_get_select_default(&pdev->dev);
	if (IS_ERR(pinctrl))
		TMR_ERR("pins are not configured from the driver");

	use_tclkin = of_get_property(pdev->dev.of_node, "use-tclkin", NULL);
	if(use_tclkin && be32_to_cpup(use_tclkin) == 1) {
		omap_dm_timer_use_tclkin(pdata);
	} else {
		TMR_INFO("using system clock");
	}

	pdata->info.mode = PPS_CAPTUREASSERT | PPS_ECHOASSERT | PPS_CANWAIT | PPS_TSFMT_TSPEC;
	pdata->info.owner = THIS_MODULE;
	snprintf(pdata->info.name, PPS_MAX_NAME_LEN - 1, "%s", pdata->timer_name);

	pdata->pps = pps_register_source(&pdata->info, PPS_CAPTUREASSERT);
	if (pdata->pps == NULL) {
		TMR_ERR("failed to register %s as PPS source", pdata->timer_name);
        pr_err("failed to register %s as PPS source", pdata->timer_name);
	} else {
        pr_err(" %s registered as PPS source", pdata->timer_name);
		// ready to go
		pdata->ready = 1;
		omap_tmrcpt_clocksource_init(pdata);

		omap_tmrcpt_enable_irq(pdata);
	}

	return 0;
}

static int omap_timer_capture_remove(struct platform_device *pdev) {
	struct omap_tmrcpt_platform_data *pdata;
	pdata = pdev->dev.platform_data;

	if(pdata) {
		omap_tmrcpt_clocksource_cleanup(pdata);

		omap_tmrcpt_cleanup_timer(pdata);

		if(pdata->pps) {
			pps_unregister_source(pdata->pps);
			pdata->pps = NULL;
		}

		devm_kfree(&pdev->dev, pdata);
		pdev->dev.platform_data = NULL;

		sysfs_remove_group(&pdev->dev.kobj, &attr_group);
	}

	platform_set_drvdata(pdev, NULL);

	return 0;
}

#ifdef CONFIG_PM_SLEEP
static int timer_counter_suspend(struct platform_device *pdev, pm_message_t mesg)
{
    struct omap_tmrcpt_platform_data *pdata = pdev->dev.platform_data;

    if (pdata) {
        pdata->state = TMRCPT_STATE_STOP;
        disable_irq(pdata->capture_timer->irq);

        omap_tmrcpt_clocksource_cleanup(pdata);
        if(pdata->pps) {
            pps_unregister_source(pdata->pps);
            pdata->pps = NULL;
        }
    }

    pinctrl_pm_select_sleep_state(&pdev->dev);

	return 0;
}

static int timer_counter_resume(struct platform_device *pdev)
{
    struct omap_tmrcpt_platform_data *pdata = pdev->dev.platform_data;

    pdata->pps = pps_register_source(&pdata->info, PPS_CAPTUREASSERT);
	if (pdata->pps == NULL) {
		TMR_ERR("failed to register %s as PPS source", pdata->timer_name);
	} else {
		pdata->ready = 1;
		omap_tmrcpt_clocksource_init(pdata);

        omap_tmrcpt_enable_irq(pdata);
//		enable_irq(pdata->capture_timer->irq);
	}

	omap_tmrcpt_setup_capture(pdata->capture_timer, pdata->direction);
	pdata->capture = 0;
	pdata->state = TMRCPT_STATE_STARTED;

    pinctrl_pm_select_default_state(&pdev->dev);

	return 0;
}
#endif

static struct platform_driver omap_timer_capture_driver = {
	.probe		= omap_timer_capture_probe,
	.remove		= omap_timer_capture_remove,
    .suspend    = timer_counter_suspend,
    .resume     = timer_counter_resume,
	.driver		= {
		.name	= MODULE_NAME,
		.owner	= THIS_MODULE,
		.of_match_table	= of_match_ptr(omap_timer_capture_dt_ids),
	},
};

module_platform_driver(omap_timer_capture_driver);
